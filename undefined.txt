
Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa "Opastusta tietokoneen käytössä (digineuvonta)", koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. Myöskin Caisa:n linkki "Tilausvaraus" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla? ended. 
Pirkkolantiellä keskikorokkeelta merkit puuttuvat ended. 
Jalkakäytävällä harmaata verkkoaitaa ilman varoitusmerkintöjä. Tässä väistetään usein väärin pysäköittjä autoja pyörällä jalankulkupuolelle. ended. 
Pysähtyminen sallittu klo 5-11, joka päivä tämä paikka on täynnä jalkakäytäville pysäköityjä autoja. Miksi tälle ei tehdä mitään? ended. 
Osoitteessa Laivurinkatu 39 VP lisäkilpi vinossa. Voisi oikaista, kiitos. ended. 
Osoitteessa Döbelninkatu 1 Suojatie liikennemerkki nurin. ended. 
Lisäkilpi katkennut. ended. 
Vettä valutetaan työmaalta keskuspuistoon, jossa se muuttaa kävelytien mitaiseksi tai jäiseksi lämpötilasta riippuen. Kuvassa näkyy letku työmaalta. ended. 
Nuoret (13-17v)toivovat , että ympäri Helsinkiä(varsinkin Kontulaan) tulisi lisää tanssisaleista joista nuoret saisivat maksuttomia vuoroja käyttöönsä. Nuorilla on vähän rahaa ja harrastukset ovat liian kalliita nuorille. Nuoret voivat itse järjestää tunteja ohjaajina .

http://ruuti.munstadi.fi/aloitteet/maksuttomia-tanssisali-vuoroja-nuorille/ ended. 
vaara jalankulkijoille ended. 
Kartanonkaari ajotie erittäin liukas. Muutenkin alueen tiet erittäin liukkaita. Voisi suolata useammin ajoteitä. ended. 
Siirtokehotuksen voimassaolo pvm päättynyt 7.4.2017. ended. 
Pysäköintikiellettymerkki kääntynyt pois osoitteesta Rinne jonne sen tulisi osoittaa.

308 ended. 
Hei,
Olen lähiaikoina törmännyt kirjaston koneilla tällaiseen ongelmaan. Olen yrittänyt katsella monia julkisia (ns.public) f-book sivuja (esim.artistien, levy-yhtiöitten, julkisyhteisöjen yms).
Ensin kone kysyy sitä kirjainkoodia ja sitten, kun sen onnistuneesti sen kirjoittaa, niin näytölle tulee ilmoitus, että pitäisi kirjautua ensin itse f-bookiin, että sivuja pääsisi katsomaan.
En ole itse f-bookissa kirjautuneena ollenkaan ja aina aiemmin näitä kaikille avoimia julkisia f-book sivuja on ongelmitta päässyt katsomaan kirjaston koneilta .
Onkohan teidän systeemeissänne tullut joku vika tämän suhteen vai onko Facebook itse muuttanut/kiristänyt systeemejään tämän suhteen. Toivottavasti tämä korjataan. Oikeasti vähän ongelmallista, jos tiedonsaantia näin rajataan. Parempi kuin noista kirjain/numerokoodeistakin päästäisiin eroon. Ova usein todella vaikeasti luettavia. ended. 
Mariankatu 11. Pysäköintikieltomerkki puuttuu. ended. 
Kumpulantien lisäksi kuoppia ratamestarinkadun ajoväylällä ended. 
Vaunu pysyväisparkkeerannut alle 5m eteen suojatiestä. Estää näkyvyyden suojatielle.

Kumpulantie ended. 
Aseman portaat mustalla jäällä kuorrutettu-vaarallisen liukkaat! ended. 
Liikennemerkki jälleen 45 asteen kulmassa kallistettuna, ennen tunneliin menoa. ended. 
Onko kuvassa olevilla lavoilla säilytys lupa kivihaantien päässä olevalla kääntöpaikalla? Haittaa kääntöpaikan käyttöä. ended. 
Pengerkatu 16 kohdalla poltettu henkilöauto ended. 
Tyhjennys ? Siltavuorenranta ended. 
Hei,

olisin tiedustellut Länsi-Herttoniemen Majavatien päässä olevien retkeilypenkkien perään. Pöytäryhmä kallion päältä on poistettu. Onko vastaavaa tulossa tilalle tai jopa muutama lisää? Tämä kohta on monen lintuharrastajan tiirailupaikka sekä lapsiperheiden suosikki. ended. 
...rikki. pohjois espa 39 ended. 
Liikennemerkki kaatunut ended. 
Liikennemerkki makaa maassa ended. 
Aitaus nurin lojumassa jalkakäytävällä. Pv 269 ended. 
Vartiokyläntien varressa luvaton kaatopaikka. ended. 
Katukiveys rikki Merimiehenkadun puolella ended. 
Mittaa 10 metriä, olisi hyvä saada pois, kiitos! ended. 
Onko mahdollista saada tehtaanpuiston kävelyteille soraa/kivetys sillä nykyisellään tiet ovat pelkkää liukasta mutasohjoa? ended. 
Katukivet ovat itsellään. Fabianinkatu 37 ended. 
Liikennemerkki nurin Sörnäisten rantatie 5 kohdalla. ended. 
Väliaikainen liikennevalo olisi siirrettävä oikealle. Nyt sen ollessa väärällä puolella joka toinen auto ajaa aamuisin punaisilla läpi ended. 
Paikassa hidastetöyssy liian korkea tehdyn kaivuutyön jälkeen. Vaarallinen autoilijoille. ended. 
Kisahallin voimistelijoiden äänentoistojärjestelmä soi todella kovalla ja häiritsee erityisesti kaiuttimien läheisten harjoituspaikkojen harjoittelurauhaa.

Kaiuttimien uudelleen sijoittaminen voisi auttaa asiaa äänen taso täytyy pitää nykyisellä tasollaan. ended. 
Mrilahdentie. Kiltomrrkki ended. 
Vanhaa tietoa ended. 
Hallainholmassa kivistä kasattu tulisija, jonka vieressä ja läheisyydessä polttopuitakin valmiina odottamassa. Sekä klapeja että pari pölliä. Voisiko ne siivota pois, paikalla ei pidä tehdä tulia. Toki vastaavia tulisijoja on pitkin Helsingin rantoja, täällä paikka vain on varsin riskialtis. ended. 
Hei. Onko Muinaisrannantie 3 kiekkopaikkoja osoittava Liikennemerkki oikeassa paikassa? ended. 
Terve!!  

Kaupungin sivuilla oleva pysäköintikartta vyöhykehinnoista ei pidä paikkaansa. 

Eikä pysäköintihinnat muutoinkaan ole perusteltuja , ei Töölössä eikä Salmisaaressa pitäisi olla I-vyöhykkeen hintoja,

Kaupunki on lähtenyt rahastukseen joka on täysin vastoin kaupunkilaisten tahtoa.

t Stadin Kundi ended. 
Harjaterästä. ended. 
Kadun pätkältä puuttuu pysäköintikieltomerkki tms, ennen kiekkopaikkoja. ended. 
Hei,

Olen muuttamassa suomeen 2 lapseni kanssa vuodenvaihteessa, ja esikoiseni (7v) on taysin englanninkielinen, ja nain ollen tarvitsee paikan englanninkieliselta ensimmaiselta luokalta. Nettisivunne ei anna minkaanlaista kunnon vastaustaa mitka koulut tarjoavat englanninkielista opetusta, ja miten hakea paikkaa naista kouuista. Voisitteko ystavallisesti antaa konkreettisia ohjeita miten jarjestaa tama asia? ended. 
Liikennemerkki kaatunut osoitteessa Ratasmyllynraitti 3. ended. 
Pysäköinti kieleletty -merkki poikki Vuorikatu 8 ended. 
Liikuntahulinat viikonloppuisin ovat lapsiperheiden sadepäivien pelastus! Kiitos niistä! ended. 
Voisiko tässä olla Jämsän kadulta teollisuuskadulla mennessä sallittuna  kaksi kääntyvää kaistaa? Nyt vain yksi vaikka teollisuuskadulla on kaksi kaistaa vastassa.

Näin kapasiteetti kaksinkertaistui. ended. 
Aluekieltomerkki puuttuu. Toisessa päässä on. ended. 
Suojatiemerkki puuttuu ended. 
Sivulta on liian vaikea löytää Hakasalmen huvilan näyttelyitä. En halua haahuilla kaiken sälän keskellä, vaan löytää Hakasalmen suoraan valikosta. Menin ensin http://hakasalmenhuvila.fi/nayttelyt/ sivulle, jonka perusteella luulin, ettei Hakasalmella enää ole omaa sivua, vaan vain tuo Kaupunginmuseo-linkki. Vasta haahuilun jälkeen huomasin, että mainitulla sivulla ON etusivu, josta pääsee varsinaiseen Hakasalmi-infoon. Kaupunginmuseon saitille Hakasalmi maastoutui liian hyvin.


   ended. 
Miksi Kallion kirjastoa remontoidaan
öisin klo 03.00 - 05.00 erittäin kovaäänisesti?
Ei saa nukuttua.

 ended. 
Mielestäni joululoma tulisi säilyttää entisen pituisena. Tätä ei pidä lyhentää. Tärkeä tauko koulutyöhön ja perheellä mahdollisuus yhteisiin lominä.. Syysloma myöskin koululaisille tarpeen viikon pituisena. ended. 
Mukaan viheralue? ended. 
Kiellon jälkeen pitäisi olla uusi merkki, maksullisuus kiellolla. ended. 
Pv/ P-merkki 4h, 6-24 puuttuu ended. 
Toisella kaistalla Lahteen päin erittäin syviä kuoppia tiessä n50m matkalla.
 ended. 
Kulosaaren puistotien kevyen liikenteen väylälle jätetty puista pudonneet lehdet. ended. 
Koirapuiston aita rikki. Aidan alle kaivettu reikä, koirat pääsevät karkuun. ended. 
Yksi mukulakivi puuttuu syvyys n. 10 cm ended. 
Hei,

Hakamäentien tunnelissa päällystevaurioita.
Molemminpuolin em. tunnelissa kaivonkansien ympäriltä alfaltti painunut alaspäin muodostaen kuoppia päälysteeseen.
Korjaus mahd. pian!!! ended. 
Kaivonkannen vieressä kuoppa asfaltissa.
Linnanherrankuja 3 ended. 
Paljon reikiä. 
 ended. 
Saisiko tänne edes sellaisia perisuomalaisia yhden hengen teräspenkkejä? ended. 
Kumpulantie 3:n kohdalla 
pasilassa asfaltissa kuoppia. ended. 
Lavan takana kaatopaikka ended. 
Uutelan nuotiopaikan rannassa havaittu jotain mönjää vedessä :( ended. 
Pysäköintikielletty-merkki väärinpäin. ended. 
Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa "Opastusta tietokoneen käytössä (digineuvonta)", koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. Myöskin Caisa:n linkki "Tilausvaraus" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla? ended. 
Jalkakäytävällä harmaata verkkoaitaa ilman varoitusmerkintöjä. Tässä väistetään usein väärin pysäköittjä autoja pyörällä jalankulkupuolelle. ended. 
Pirkkolantiellä keskikorokkeelta merkit puuttuvat ended. 
Osoitteessa Döbelninkatu 1 Suojatie liikennemerkki nurin. ended. 
vaara jalankulkijoille ended. 
Kartanonkaari ajotie erittäin liukas. Muutenkin alueen tiet erittäin liukkaita. Voisi suolata useammin ajoteitä. ended. 
Siirtokehotuksen voimassaolo pvm päättynyt 7.4.2017. ended. 
Pysäköintikiellettymerkki kääntynyt pois osoitteesta Rinne jonne sen tulisi osoittaa.

308 ended. 
Mariankatu 11. Pysäköintikieltomerkki puuttuu. ended. 
Kumpulantien lisäksi kuoppia ratamestarinkadun ajoväylällä ended. 
Aseman portaat mustalla jäällä kuorrutettu-vaarallisen liukkaat! ended. 
Liikennemerkki jälleen 45 asteen kulmassa kallistettuna, ennen tunneliin menoa. ended. 
Onko kuvassa olevilla lavoilla säilytys lupa kivihaantien päässä olevalla kääntöpaikalla? Haittaa kääntöpaikan käyttöä. ended. 
Pengerkatu 16 kohdalla poltettu henkilöauto ended. 
...rikki. pohjois espa 39 ended. 
Liikennemerkki kaatunut ended. 
Liikennemerkki makaa maassa ended. 
Aitaus nurin lojumassa jalkakäytävällä. Pv 269 ended. 
Vartiokyläntien varressa luvaton kaatopaikka. ended. 
Mittaa 10 metriä, olisi hyvä saada pois, kiitos! ended. 
Onko mahdollista saada tehtaanpuiston kävelyteille soraa/kivetys sillä nykyisellään tiet ovat pelkkää liukasta mutasohjoa? ended. 
Katukivet ovat itsellään. Fabianinkatu 37 ended. 
Liikennemerkki nurin Sörnäisten rantatie 5 kohdalla. ended. 
Mrilahdentie. Kiltomrrkki ended. 
Vanhaa tietoa ended. 
Päivittäin Linnunlauluntiellä liikkuvana olen kiinnittänyt huomiota siihen, että edelleen klo 8-17 välillä on noin puolen tusinaa autoa parkissa Linnunlauluntiellä ilman kiekkoa. Meno on kyllä rauhoittunut edellisen sakotuskierroksen jälkeen.

 ended. 
Hei. Onko Muinaisrannantie 3 kiekkopaikkoja osoittava Liikennemerkki oikeassa paikassa? ended. 
Harjaterästä. ended. 
Kadun pätkältä puuttuu pysäköintikieltomerkki tms, ennen kiekkopaikkoja. ended. 
Hei,

Olen muuttamassa suomeen 2 lapseni kanssa vuodenvaihteessa, ja esikoiseni (7v) on taysin englanninkielinen, ja nain ollen tarvitsee paikan englanninkieliselta ensimmaiselta luokalta. Nettisivunne ei anna minkaanlaista kunnon vastaustaa mitka koulut tarjoavat englanninkielista opetusta, ja miten hakea paikkaa naista kouuista. Voisitteko ystavallisesti antaa konkreettisia ohjeita miten jarjestaa tama asia? ended. 
Liikennemerkki kaatunut osoitteessa Ratasmyllynraitti 3. ended. 
Liikuntahulinat viikonloppuisin ovat lapsiperheiden sadepäivien pelastus! Kiitos niistä! ended. 
Aluekieltomerkki puuttuu. Toisessa päässä on. ended. 
Suojatiemerkki puuttuu ended. 
Sivulta on liian vaikea löytää Hakasalmen huvilan näyttelyitä. En halua haahuilla kaiken sälän keskellä, vaan löytää Hakasalmen suoraan valikosta. Menin ensin http://hakasalmenhuvila.fi/nayttelyt/ sivulle, jonka perusteella luulin, ettei Hakasalmella enää ole omaa sivua, vaan vain tuo Kaupunginmuseo-linkki. Vasta haahuilun jälkeen huomasin, että mainitulla sivulla ON etusivu, josta pääsee varsinaiseen Hakasalmi-infoon. Kaupunginmuseon saitille Hakasalmi maastoutui liian hyvin.


   ended. 
Miksi Kallion kirjastoa remontoidaan
öisin klo 03.00 - 05.00 erittäin kovaäänisesti?
Ei saa nukuttua.

 ended. 
Hei,

Hakamäentien tunnelissa päällystevaurioita.
Molemminpuolin em. tunnelissa kaivonkansien ympäriltä alfaltti painunut alaspäin muodostaen kuoppia päälysteeseen.
Korjaus mahd. pian!!! ended. 
Toisella kaistalla Lahteen päin erittäin syviä kuoppia tiessä n50m matkalla.
 ended. 
Pv/ P-merkki 4h, 6-24 puuttuu ended. 
Mukaan viheralue? ended. 
Kiellon jälkeen pitäisi olla uusi merkki, maksullisuus kiellolla. ended. 
Pysäköintikielletty-merkki väärinpäin. ended. 
Koirapuiston aita rikki. Aidan alle kaivettu reikä, koirat pääsevät karkuun. ended. 
Yksi mukulakivi puuttuu syvyys n. 10 cm ended. 
Kaivonkannen vieressä kuoppa asfaltissa.
Linnanherrankuja 3 ended. 
Kumpulantie 3:n kohdalla 
pasilassa asfaltissa kuoppia. ended. 
Paljon reikiä. 
 ended. 
Saisiko tänne edes sellaisia perisuomalaisia yhden hengen teräspenkkejä? ended. 
Lavan takana kaatopaikka ended. 
Uutelan nuotiopaikan rannassa havaittu jotain mönjää vedessä :( ended. 
Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa "Opastusta tietokoneen käytössä (digineuvonta)", koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. Myöskin Caisa:n linkki "Tilausvaraus" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla? ended. 
Jalkakäytävällä harmaata verkkoaitaa ilman varoitusmerkintöjä. Tässä väistetään usein väärin pysäköittjä autoja pyörällä jalankulkupuolelle. ended. 
Pirkkolantiellä keskikorokkeelta merkit puuttuvat ended. 
Osoitteessa Döbelninkatu 1 Suojatie liikennemerkki nurin. ended. 
vaara jalankulkijoille ended. 
Kartanonkaari ajotie erittäin liukas. Muutenkin alueen tiet erittäin liukkaita. Voisi suolata useammin ajoteitä. ended. 
Siirtokehotuksen voimassaolo pvm päättynyt 7.4.2017. ended. 
Pysäköintikiellettymerkki kääntynyt pois osoitteesta Rinne jonne sen tulisi osoittaa.

308 ended. 
Mariankatu 11. Pysäköintikieltomerkki puuttuu. ended. 
Kumpulantien lisäksi kuoppia ratamestarinkadun ajoväylällä ended. 
Aseman portaat mustalla jäällä kuorrutettu-vaarallisen liukkaat! ended. 
Liikennemerkki jälleen 45 asteen kulmassa kallistettuna, ennen tunneliin menoa. ended. 
Onko kuvassa olevilla lavoilla säilytys lupa kivihaantien päässä olevalla kääntöpaikalla? Haittaa kääntöpaikan käyttöä. ended. 
Pengerkatu 16 kohdalla poltettu henkilöauto ended. 
Liikennemerkki kaatunut ended. 
...rikki. pohjois espa 39 ended. 
Liikennemerkki makaa maassa ended. 
Aitaus nurin lojumassa jalkakäytävällä. Pv 269 ended. 
Vartiokyläntien varressa luvaton kaatopaikka. ended. 
Mittaa 10 metriä, olisi hyvä saada pois, kiitos! ended. 
Onko mahdollista saada tehtaanpuiston kävelyteille soraa/kivetys sillä nykyisellään tiet ovat pelkkää liukasta mutasohjoa? ended. 
Katukivet ovat itsellään. Fabianinkatu 37 ended. 
Liikennemerkki nurin Sörnäisten rantatie 5 kohdalla. ended. 
Mrilahdentie. Kiltomrrkki ended. 
Vanhaa tietoa ended. 
Päivittäin Linnunlauluntiellä liikkuvana olen kiinnittänyt huomiota siihen, että edelleen klo 8-17 välillä on noin puolen tusinaa autoa parkissa Linnunlauluntiellä ilman kiekkoa. Meno on kyllä rauhoittunut edellisen sakotuskierroksen jälkeen.

 ended. 
Hei. Onko Muinaisrannantie 3 kiekkopaikkoja osoittava Liikennemerkki oikeassa paikassa? ended. 
Harjaterästä. ended. 
Kadun pätkältä puuttuu pysäköintikieltomerkki tms, ennen kiekkopaikkoja. ended. 
Hei,

Olen muuttamassa suomeen 2 lapseni kanssa vuodenvaihteessa, ja esikoiseni (7v) on taysin englanninkielinen, ja nain ollen tarvitsee paikan englanninkieliselta ensimmaiselta luokalta. Nettisivunne ei anna minkaanlaista kunnon vastaustaa mitka koulut tarjoavat englanninkielista opetusta, ja miten hakea paikkaa naista kouuista. Voisitteko ystavallisesti antaa konkreettisia ohjeita miten jarjestaa tama asia? ended. 
Liikennemerkki kaatunut osoitteessa Ratasmyllynraitti 3. ended. 
Liikuntahulinat viikonloppuisin ovat lapsiperheiden sadepäivien pelastus! Kiitos niistä! ended. 
Aluekieltomerkki puuttuu. Toisessa päässä on. ended. 
Suojatiemerkki puuttuu ended. 
Sivulta on liian vaikea löytää Hakasalmen huvilan näyttelyitä. En halua haahuilla kaiken sälän keskellä, vaan löytää Hakasalmen suoraan valikosta. Menin ensin http://hakasalmenhuvila.fi/nayttelyt/ sivulle, jonka perusteella luulin, ettei Hakasalmella enää ole omaa sivua, vaan vain tuo Kaupunginmuseo-linkki. Vasta haahuilun jälkeen huomasin, että mainitulla sivulla ON etusivu, josta pääsee varsinaiseen Hakasalmi-infoon. Kaupunginmuseon saitille Hakasalmi maastoutui liian hyvin.


   ended. 
Miksi Kallion kirjastoa remontoidaan
öisin klo 03.00 - 05.00 erittäin kovaäänisesti?
Ei saa nukuttua.

 ended. 
Hei,

Hakamäentien tunnelissa päällystevaurioita.
Molemminpuolin em. tunnelissa kaivonkansien ympäriltä alfaltti painunut alaspäin muodostaen kuoppia päälysteeseen.
Korjaus mahd. pian!!! ended. 
Toisella kaistalla Lahteen päin erittäin syviä kuoppia tiessä n50m matkalla.
 ended. 
Pv/ P-merkki 4h, 6-24 puuttuu ended. 
Mukaan viheralue? ended. 
Kiellon jälkeen pitäisi olla uusi merkki, maksullisuus kiellolla. ended. 
Pysäköintikielletty-merkki väärinpäin. ended. 
Koirapuiston aita rikki. Aidan alle kaivettu reikä, koirat pääsevät karkuun. ended. 
Yksi mukulakivi puuttuu syvyys n. 10 cm ended. 
Kaivonkannen vieressä kuoppa asfaltissa.
Linnanherrankuja 3 ended. 
Kumpulantie 3:n kohdalla 
pasilassa asfaltissa kuoppia. ended. 
Paljon reikiä. 
 ended. 
Saisiko tänne edes sellaisia perisuomalaisia yhden hengen teräspenkkejä? ended. 
Lavan takana kaatopaikka ended. 
Uutelan nuotiopaikan rannassa havaittu jotain mönjää vedessä :( ended. 
Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa "Opastusta tietokoneen käytössä (digineuvonta)", koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. Myöskin Caisa:n linkki "Tilausvaraus" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla? ended. 
Jalkakäytävällä harmaata verkkoaitaa ilman varoitusmerkintöjä. Tässä väistetään usein väärin pysäköittjä autoja pyörällä jalankulkupuolelle. ended. 
Pirkkolantiellä keskikorokkeelta merkit puuttuvat ended. 
Osoitteessa Döbelninkatu 1 Suojatie liikennemerkki nurin. ended. 
vaara jalankulkijoille ended. 
Kartanonkaari ajotie erittäin liukas. Muutenkin alueen tiet erittäin liukkaita. Voisi suolata useammin ajoteitä. ended. 
Siirtokehotuksen voimassaolo pvm päättynyt 7.4.2017. ended. 
Pysäköintikiellettymerkki kääntynyt pois osoitteesta Rinne jonne sen tulisi osoittaa.

308 ended. 
Mariankatu 11. Pysäköintikieltomerkki puuttuu. ended. 
Kumpulantien lisäksi kuoppia ratamestarinkadun ajoväylällä ended. 
Aseman portaat mustalla jäällä kuorrutettu-vaarallisen liukkaat! ended. 
Liikennemerkki jälleen 45 asteen kulmassa kallistettuna, ennen tunneliin menoa. ended. 
Onko kuvassa olevilla lavoilla säilytys lupa kivihaantien päässä olevalla kääntöpaikalla? Haittaa kääntöpaikan käyttöä. ended. 
Pengerkatu 16 kohdalla poltettu henkilöauto ended. 
...rikki. pohjois espa 39 ended. 
Liikennemerkki kaatunut ended. 
Liikennemerkki makaa maassa ended. 
Aitaus nurin lojumassa jalkakäytävällä. Pv 269 ended. 
Vartiokyläntien varressa luvaton kaatopaikka. ended. 
Mittaa 10 metriä, olisi hyvä saada pois, kiitos! ended. 
Onko mahdollista saada tehtaanpuiston kävelyteille soraa/kivetys sillä nykyisellään tiet ovat pelkkää liukasta mutasohjoa? ended. 
Katukivet ovat itsellään. Fabianinkatu 37 ended. 
Liikennemerkki nurin Sörnäisten rantatie 5 kohdalla. ended. 
Mrilahdentie. Kiltomrrkki ended. 
Vanhaa tietoa ended. 
Päivittäin Linnunlauluntiellä liikkuvana olen kiinnittänyt huomiota siihen, että edelleen klo 8-17 välillä on noin puolen tusinaa autoa parkissa Linnunlauluntiellä ilman kiekkoa. Meno on kyllä rauhoittunut edellisen sakotuskierroksen jälkeen.

 ended. 
Hei. Onko Muinaisrannantie 3 kiekkopaikkoja osoittava Liikennemerkki oikeassa paikassa? ended. 
Harjaterästä. ended. 
Kadun pätkältä puuttuu pysäköintikieltomerkki tms, ennen kiekkopaikkoja. ended. 
Hei,

Olen muuttamassa suomeen 2 lapseni kanssa vuodenvaihteessa, ja esikoiseni (7v) on taysin englanninkielinen, ja nain ollen tarvitsee paikan englanninkieliselta ensimmaiselta luokalta. Nettisivunne ei anna minkaanlaista kunnon vastaustaa mitka koulut tarjoavat englanninkielista opetusta, ja miten hakea paikkaa naista kouuista. Voisitteko ystavallisesti antaa konkreettisia ohjeita miten jarjestaa tama asia? ended. 
Liikennemerkki kaatunut osoitteessa Ratasmyllynraitti 3. ended. 
Liikuntahulinat viikonloppuisin ovat lapsiperheiden sadepäivien pelastus! Kiitos niistä! ended. 
Aluekieltomerkki puuttuu. Toisessa päässä on. ended. 
Suojatiemerkki puuttuu ended. 
Sivulta on liian vaikea löytää Hakasalmen huvilan näyttelyitä. En halua haahuilla kaiken sälän keskellä, vaan löytää Hakasalmen suoraan valikosta. Menin ensin http://hakasalmenhuvila.fi/nayttelyt/ sivulle, jonka perusteella luulin, ettei Hakasalmella enää ole omaa sivua, vaan vain tuo Kaupunginmuseo-linkki. Vasta haahuilun jälkeen huomasin, että mainitulla sivulla ON etusivu, josta pääsee varsinaiseen Hakasalmi-infoon. Kaupunginmuseon saitille Hakasalmi maastoutui liian hyvin.


   ended. 
Miksi Kallion kirjastoa remontoidaan
öisin klo 03.00 - 05.00 erittäin kovaäänisesti?
Ei saa nukuttua.

 ended. 
Hei,

Hakamäentien tunnelissa päällystevaurioita.
Molemminpuolin em. tunnelissa kaivonkansien ympäriltä alfaltti painunut alaspäin muodostaen kuoppia päälysteeseen.
Korjaus mahd. pian!!! ended. 
Toisella kaistalla Lahteen päin erittäin syviä kuoppia tiessä n50m matkalla.
 ended. 
Pv/ P-merkki 4h, 6-24 puuttuu ended. 
Mukaan viheralue? ended. 
Kiellon jälkeen pitäisi olla uusi merkki, maksullisuus kiellolla. ended. 
Pysäköintikielletty-merkki väärinpäin. ended. 
Koirapuiston aita rikki. Aidan alle kaivettu reikä, koirat pääsevät karkuun. ended. 
Yksi mukulakivi puuttuu syvyys n. 10 cm ended. 
Kaivonkannen vieressä kuoppa asfaltissa.
Linnanherrankuja 3 ended. 
Kumpulantie 3:n kohdalla 
pasilassa asfaltissa kuoppia. ended. 
Paljon reikiä. 
 ended. 
Saisiko tänne edes sellaisia perisuomalaisia yhden hengen teräspenkkejä? ended. 
Lavan takana kaatopaikka ended. 
Uutelan nuotiopaikan rannassa havaittu jotain mönjää vedessä :( ended. 
Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa "Opastusta tietokoneen käytössä (digineuvonta)", koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. Myöskin Caisa:n linkki "Tilausvaraus" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla? ended. 
Jalkakäytävällä harmaata verkkoaitaa ilman varoitusmerkintöjä. Tässä väistetään usein väärin pysäköittjä autoja pyörällä jalankulkupuolelle. ended. 
Pirkkolantiellä keskikorokkeelta merkit puuttuvat ended. 
Osoitteessa Döbelninkatu 1 Suojatie liikennemerkki nurin. ended. 
vaara jalankulkijoille ended. 
Kartanonkaari ajotie erittäin liukas. Muutenkin alueen tiet erittäin liukkaita. Voisi suolata useammin ajoteitä. ended. 
Siirtokehotuksen voimassaolo pvm päättynyt 7.4.2017. ended. 
Pysäköintikiellettymerkki kääntynyt pois osoitteesta Rinne jonne sen tulisi osoittaa.

308 ended. 
Mariankatu 11. Pysäköintikieltomerkki puuttuu. ended. 
Kumpulantien lisäksi kuoppia ratamestarinkadun ajoväylällä ended. 
Aseman portaat mustalla jäällä kuorrutettu-vaarallisen liukkaat! ended. 
Liikennemerkki jälleen 45 asteen kulmassa kallistettuna, ennen tunneliin menoa. ended. 
Onko kuvassa olevilla lavoilla säilytys lupa kivihaantien päässä olevalla kääntöpaikalla? Haittaa kääntöpaikan käyttöä. ended. 
Pengerkatu 16 kohdalla poltettu henkilöauto ended. 
...rikki. pohjois espa 39 ended. 
Liikennemerkki kaatunut ended. 
Liikennemerkki makaa maassa ended. 
Aitaus nurin lojumassa jalkakäytävällä. Pv 269 ended. 
Vartiokyläntien varressa luvaton kaatopaikka. ended. 
Mittaa 10 metriä, olisi hyvä saada pois, kiitos! ended. 
Onko mahdollista saada tehtaanpuiston kävelyteille soraa/kivetys sillä nykyisellään tiet ovat pelkkää liukasta mutasohjoa? ended. 
Katukivet ovat itsellään. Fabianinkatu 37 ended. 
Liikennemerkki nurin Sörnäisten rantatie 5 kohdalla. ended. 
Mrilahdentie. Kiltomrrkki ended. 
Vanhaa tietoa ended. 
Päivittäin Linnunlauluntiellä liikkuvana olen kiinnittänyt huomiota siihen, että edelleen klo 8-17 välillä on noin puolen tusinaa autoa parkissa Linnunlauluntiellä ilman kiekkoa. Meno on kyllä rauhoittunut edellisen sakotuskierroksen jälkeen.

 ended. 
Hei. Onko Muinaisrannantie 3 kiekkopaikkoja osoittava Liikennemerkki oikeassa paikassa? ended. 
Harjaterästä. ended. 
Kadun pätkältä puuttuu pysäköintikieltomerkki tms, ennen kiekkopaikkoja. ended. 
Hei,

Olen muuttamassa suomeen 2 lapseni kanssa vuodenvaihteessa, ja esikoiseni (7v) on taysin englanninkielinen, ja nain ollen tarvitsee paikan englanninkieliselta ensimmaiselta luokalta. Nettisivunne ei anna minkaanlaista kunnon vastaustaa mitka koulut tarjoavat englanninkielista opetusta, ja miten hakea paikkaa naista kouuista. Voisitteko ystavallisesti antaa konkreettisia ohjeita miten jarjestaa tama asia? ended. 
Liikennemerkki kaatunut osoitteessa Ratasmyllynraitti 3. ended. 
Liikuntahulinat viikonloppuisin ovat lapsiperheiden sadepäivien pelastus! Kiitos niistä! ended. 
Aluekieltomerkki puuttuu. Toisessa päässä on. ended. 
Suojatiemerkki puuttuu ended. 
Sivulta on liian vaikea löytää Hakasalmen huvilan näyttelyitä. En halua haahuilla kaiken sälän keskellä, vaan löytää Hakasalmen suoraan valikosta. Menin ensin http://hakasalmenhuvila.fi/nayttelyt/ sivulle, jonka perusteella luulin, ettei Hakasalmella enää ole omaa sivua, vaan vain tuo Kaupunginmuseo-linkki. Vasta haahuilun jälkeen huomasin, että mainitulla sivulla ON etusivu, josta pääsee varsinaiseen Hakasalmi-infoon. Kaupunginmuseon saitille Hakasalmi maastoutui liian hyvin.


   ended. 
Miksi Kallion kirjastoa remontoidaan
öisin klo 03.00 - 05.00 erittäin kovaäänisesti?
Ei saa nukuttua.

 ended. 
Hei,

Hakamäentien tunnelissa päällystevaurioita.
Molemminpuolin em. tunnelissa kaivonkansien ympäriltä alfaltti painunut alaspäin muodostaen kuoppia päälysteeseen.
Korjaus mahd. pian!!! ended. 
Toisella kaistalla Lahteen päin erittäin syviä kuoppia tiessä n50m matkalla.
 ended. 
Pv/ P-merkki 4h, 6-24 puuttuu ended. 
Mukaan viheralue? ended. 
Kiellon jälkeen pitäisi olla uusi merkki, maksullisuus kiellolla. ended. 
Pysäköintikielletty-merkki väärinpäin. ended. 
Koirapuiston aita rikki. Aidan alle kaivettu reikä, koirat pääsevät karkuun. ended. 
Yksi mukulakivi puuttuu syvyys n. 10 cm ended. 
Kaivonkannen vieressä kuoppa asfaltissa.
Linnanherrankuja 3 ended. 
Kumpulantie 3:n kohdalla 
pasilassa asfaltissa kuoppia. ended. 
Paljon reikiä. 
 ended. 
Saisiko tänne edes sellaisia perisuomalaisia yhden hengen teräspenkkejä? ended. 
Lavan takana kaatopaikka ended. 
Uutelan nuotiopaikan rannassa havaittu jotain mönjää vedessä :( ended. 