const request = require('request');
const rp = require('request-promise');

url = 'https://translation.googleapis.com/language/translate/v2';
apiKey = process.env.GOOGLE_API;

module.exports.getTranslation = function (data, callback) {
    request({
        method: 'GET',
        url: url + '?key=' + apiKey + '&source=' + data.source + '&target=' + data.target + '&q=' + encodeURI(data.message) + '&model=' + data.model
    }, callback);
};

function parseXHTMLString (text) {
    return text.replace(/&#39;/g, '\'')
        .replace(/&amp;/g, '&')
        .replace(/&quot;/g, '"')
        .replace(/&gt;/g, '>')
        .replace(/&lt;/g, '<')
        .replace(/"/g, '');
}

module.exports.parseXHTMLString = {
    parseXHTMLString
};

module.exports.getTranslation2 = function (data) {
    let options = {
        method: 'GET',
        url: url + '?key=' + apiKey + '&source=' + data.source + '&target=' + data.target + '&q=' + encodeURI(data.message) + '&model=' + data.model,
        resolveWithFullResponse: true
    };

    return rp(options).then(function (result) {

        let parsedBody = null;
        try {
            parsedBody = JSON.parse(result.body);
        } catch (e) {
            console.log('Could not parse response from Google: ' + (result || 'null'));
            return new Error('Parse error');
        }
        if (!parsedBody.data.translations[0].translatedText) return new Error('No translation');

        return Promise.resolve(parseXHTMLString(parsedBody.data.translations[0].translatedText));
    }).catch(function (error) {
        return Promise.reject(error);
    });
};

module.exports.getData = function () {
    let options = {
        method: 'GET',
        url: 'https://asiointi.hel.fi/palautews/rest/v1/requests.json?status=open',
        resolveWithFullResponse: true
    };

    return rp(options).then(function (result) {

        let parsedBody = null;
        try {
            parsedBody = JSON.parse(result.body);
        } catch (e) {
            console.log('Could not parse response from Google: ' + (result || 'null'));
            return new Error('Parse error');
        }

        return Promise.resolve(parsedBody);
    }).catch(function (error) {
        return Promise.reject(error);
    });
};