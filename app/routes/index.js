"use strict";
const cors = require('cors');

module.exports = function (app) {
    app.options('*', cors()); // Include before other routes
    app.use('/api/', require('./api/index')());

};