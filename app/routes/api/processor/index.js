"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/processor.js');

module.exports = function () {
    router.post('', ctrl.process);
    return router;
};