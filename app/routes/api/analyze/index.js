"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/analyze.js');

module.exports = function () {
    router.get('', ctrl.get);
    router.post('', ctrl.analyze);
    return router;
};