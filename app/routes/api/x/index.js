"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/x');

module.exports = function () {

    router.get('', ctrl.invalidRequest);
    router.post('', ctrl.invalidRequest);
    router.put('', ctrl.invalidRequest);
    router.delete('', ctrl.invalidRequest);

    router.get('/:id', ctrl.invalidRequest);
    router.post('/:id', ctrl.invalidRequest);
    router.put('/:id', ctrl.invalidRequest);
    router.delete('/:id', ctrl.invalidRequest);

    return router;
};