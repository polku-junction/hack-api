"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/pipe.js');

module.exports = function () {
    router.get('', ctrl.get);
    router.post('', ctrl.pipe);
    return router;
};