"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/translate');

module.exports = function () {
    router.post('', ctrl.translate);
    return router;
};