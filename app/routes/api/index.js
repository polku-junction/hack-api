"use strict";
const router = require('express').Router();

module.exports = function () {

    router.use('/x/', require('./x/index')());
    router.use('/translate/', require('./translate/index')());
    router.use('/processor/', require('./processor/index')());
    router.use('/keyword_extractor/', require('./keyword_extractor/index')());
    router.use('/pipe/', require('./pipe/index')());
    router.use('/analyze/', require('./analyze/index')());
    router.use('/costs/', require('./costs/index')());
    return router;
};