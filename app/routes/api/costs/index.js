"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/costs.js');

module.exports = function () {
    router.get('', ctrl.get);
    return router;
};