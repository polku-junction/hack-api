"use strict";

"use strict";
const router = require('express').Router();
const ctrl = require('../../../controllers/keyword_extractor');

module.exports = function () {
    router.post('', ctrl.categorizer);
    router.get('', ctrl.existing);
    return router;
};