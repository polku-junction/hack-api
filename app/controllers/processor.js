"use strict";
const fs = require('fs');
const NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
const getTranslation2 = require('../lib/google-translate').getTranslation2;

/*

var nlu = new NaturalLanguageUnderstandingV1({
    "username": process.env.SERVICE_NAME_USERNAME,
    "password": process.env.SERVICE_NAME_PASSWORD,
    version_date: NaturalLanguageUnderstandingV1.VERSION_DATE_2017_02_27
});


let data = {};
data.message = "Miksi Palvelukartta.hel.fi:ssä lukee, että teiltä saa \"Opastusta tietokoneen käytössä (digineuvonta)\", " +
    "koska sellaista palvelua teillä ei ole? Soitin ja kysyin tänään ja minulle sanottiin, ettei saa neuvontaa  tietokoneen käytössä. " +
    "Myöskin Caisa:n linkki \"Tilausvaraus\" vie virhesivulle. Miksi Caisa ei pidä näitä  ajan tasalla?" ;
data.model = 'nmt';
data.target = 'en';
data.source = 'fi';



getTranslation2(data).then(result => {

    return new Promise(resolve => {

        return nlu.analyze({
            'html': result, // Buffer or String
            'features': {
                'emotion':{},
            }
        }, function(err, response) {
            if (err) {
                resolve(err);

            } else {
                resolve(response.emotion.document.emotion);

            }
        });
    });

}).then(result => {
    console.log("success 2 " + JSON.stringify(result));

}).catch(error => {
    console.log("got this error " + error);
})


*/

module.exports.process = function (req, res) {
    nlu.analyze({
        'html': req.body.message, // Buffer or String
        'features': {
            'emotion':{}
        }
    }, function(err, response) {
        if (err) {
            return res.status(500).send({success: false, message: err.message});
        } else {
            console.log(JSON.stringify(response, null, 2));
            res.status(200).send({success: true, message: response});
        }
    });
};