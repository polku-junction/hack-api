"use strict";
const randomLocation = require('random-location');
const proj4 = require('proj4');
const util = require('util');
const async = require('async');
const rp = require('request-promise');
const getTranslation2 = require('../lib/google-translate').getTranslation2;
const getData = require('../lib/google-translate').getData;
const parseXHTMLString = require('../lib/google-translate').parseXHTMLString;
const naturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
/*
proj4.defs["EPSG:3132"] = "+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs";
proj4.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

let firstProjection = "+proj=tmerc +lat_0=0 +lon_0=25 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs";
let secondProjection = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
*/

/*
let costs = [
    {long: 25498254, lat: 6675259, cost: 30},
    {long: 25501798, lat: 6675850, cost: 41.7},
    {long: 25497379, lat: 6674530, cost: 49},
    {long: 25501714, lat: 6675613, cost: 82.1},
    {long: 25500700, lat: 6683512, cost: 83.4},
    {long: 25496056, lat: 6673156, cost: 131.08},
    {long: 25494106, lat: 6680849, cost: 144.51},
    {long: 25493321, lat: 6680749, cost: 266.32},
    {long: 25504413, lat: 6677384, cost: 297.74},
    {long: 25496493, lat: 6672975, cost: 310.62},
    {long: 25498686, lat: 6679848, cost: 365.4},
    {long: 25506130, lat: 6680693, cost: 388.3},
    {long: 25501695, lat: 6675675, cost: 434.1},
    {long: 25493119, lat: 6676484, cost: 445.8},
    {long: 25500535, lat: 6679397, cost: 457.7},
    {long: 25503474, lat: 6676882, cost: 537.46},
    {long: 25507952, lat: 6677037, cost: 544.7},
    {long: 25493331, lat: 6677037, cost: 554.66},
    {long: 25507164, lat: 6678718, cost: 602.97},
    {long: 25497193, lat: 6679792, cost: 688.22},
    {long: 25505584, lat: 6680928, cost: 738.58},
    {long: 25493939, lat: 6675828, cost: 790.48},
    {long: 25501673, lat: 6679797, cost: 1000},
    {long: 25504538, lat: 6677695, cost: 1000},
    {long: 25497243, lat: 6673762, cost: 1069.71},
    {long: 25498683, lat: 6674986, cost: 1117.5},
    {long: 25504750, lat: 6680415, cost: 1137.77},
    {long: 25493344, lat: 6679802, cost: 1180},
    {long: 25492753, lat: 6678333, cost: 1204.6},
    {long: 25502939, lat: 6673916, cost: 1237.47},
    {long: 25493776, lat: 6670536, cost: 1247.6},
    {long: 25504459, lat: 6678713, cost: 1268.94},
    {long: 25507748, lat: 6676704, cost: 1318.97},
    {long: 25498846, lat: 6677193, cost: 1378.52},
    {long: 25508151, lat: 6678011, cost: 1410.86},
    {long: 25495235, lat: 6672091, cost: 1515.35},
    {long: 25497373, lat: 6675197, cost: 1573.2},
    {long: 25496662, lat: 6672571, cost: 1626.41},
    {long: 25496174, lat: 6676113, cost: 1636.16},
    {long: 25492099, lat: 6677206, cost: 1649.61},
    {long: 25502605, lat: 6672345, cost: 1678.48},
    {long: 25492255, lat: 6682123, cost: 1686.58},
    {long: 25503418, lat: 6676341, cost: 1813.1},
    {long: 25498532, lat: 6675831, cost: 1820.32},
    {long: 25505036, lat: 6678001, cost: 2234.17},
    {long: 25504333, lat: 6677658, cost: 2255.16},
    {long: 25497918, lat: 6675228, cost: 2326.24},
    {long: 25497776, lat: 6678259, cost: 2364.18},
    {long: 25497616, lat: 6677812, cost: 2630.61},
    {long: 25497558, lat: 6674387, cost: 2789.7},
    {long: 25497111, lat: 6672528, cost: 2810.94},
    {long: 25497199, lat: 6674409, cost: 2893.74},
    {long: 25502688, lat: 6672801, cost: 3362.43},
    {long: 25502454, lat: 6677067, cost: 3410.4},
    {long: 25503827, lat: 6683091, cost: 3529.58},
    {long: 25494010, lat: 6680088, cost: 3757.5},
    {long: 25502770, lat: 6684199, cost: 3826},
    {long: 25494042, lat: 6677762, cost: 3952.15},
    {long: 25492277, lat: 6682130, cost: 4136.94},
    {long: 25504416, lat: 6677767, cost: 4832.05},
    {long: 25495378, lat: 6677222, cost: 6097.18},
    {long: 25500734, lat: 6675099, cost: 6475.64},
    {long: 25501770, lat: 6676888, cost: 6697.01},
    {long: 25495537, lat: 6673439, cost: 7604.46},
    {long: 25504859, lat: 6680507, cost: 8733.46},
    {long: 25493887, lat: 6680707, cost: 9163.99},
    {long: 25496356, lat: 6673002, cost: 9816.62},
    {long: 25507060, lat: 6679798, cost: 16565.72}
];

const P = {
    latitude: 60.208014,
    longitude: 24.9691243
};

const R = 10000; // meters

for (let j = 0; j < costs.length; j++) {
    costs[j].long = randomLocation.randomCirclePoint(P, R).longitude;
    costs[j].lat = randomLocation.randomCirclePoint(P, R).latitude;
}

console.log(costs);

*/

const costs = [{long: 25.138802527850615, lat: 60.153524389591986, cost: 30},
    {long: 25.001677427848584, lat: 60.27056360376356, cost: 41.7},
    {long: 24.852672860873774, lat: 60.16061611555587, cost: 49},
    {long: 25.11647279119077, lat: 60.266585101383676, cost: 82.1},
    {long: 25.03129392313785, lat: 60.23175597875892, cost: 83.4},
    {
        long: 24.836581467894682,
        lat: 60.22256874176127,
        cost: 131.08
    },
    {
        long: 24.98081069011713,
        lat: 60.296716666542295,
        cost: 144.51
    },
    {
        long: 25.028546004338196,
        lat: 60.22337564707647,
        cost: 266.32
    },
    {
        long: 24.981149646995366,
        lat: 60.12185298714674,
        cost: 297.74
    },
    {
        long: 24.972487236848437,
        lat: 60.17655698074695,
        cost: 310.62
    },
    {
        long: 25.042342054198603,
        lat: 60.273520578211766,
        cost: 365.4
    },
    {long: 24.860333305174017, lat: 60.13953294541308, cost: 388.3},
    {long: 25.093202791865096, lat: 60.21404644758701, cost: 434.1},
    {
        long: 24.908473597324424,
        lat: 60.205547861956035,
        cost: 445.8
    },
    {long: 24.85507750195639, lat: 60.17142266515811, cost: 457.7},
    {
        long: 25.062806173109927,
        lat: 60.200655715970406,
        cost: 537.46
    },
    {long: 25.008005461667583, lat: 60.2235132856838, cost: 544.7},
    {
        long: 24.906740442592618,
        lat: 60.261087894166145,
        cost: 554.66
    },
    {
        long: 24.91724635917615,
        lat: 60.249640700342944,
        cost: 602.97
    },
    {
        long: 24.855429956100384,
        lat: 60.20509685123375,
        cost: 688.22
    },
    {
        long: 24.975185442285532,
        lat: 60.251718614385744,
        cost: 738.58
    },
    {
        long: 25.083979575016652,
        lat: 60.16341996858271,
        cost: 790.48
    },
    {long: 24.885115579173487, lat: 60.16485586978598, cost: 1000},
    {long: 25.08931259821139, lat: 60.19230381346596, cost: 1000},
    {
        long: 24.853216197304235,
        lat: 60.157861678579266,
        cost: 1069.71
    },
    {long: 24.90187944792612, lat: 60.16195745035581, cost: 1117.5},
    {
        long: 24.99265136213281,
        lat: 60.210279237344835,
        cost: 1137.77
    },
    {long: 24.91188006958039, lat: 60.253111497661905, cost: 1180},
    {
        long: 24.932744493397244,
        lat: 60.21322041417372,
        cost: 1204.6
    },
    {
        long: 24.822484722370564,
        lat: 60.14888716750427,
        cost: 1237.47
    },
    {long: 24.97443848086263, lat: 60.24578294919972, cost: 1247.6},
    {
        long: 24.92530017152326,
        lat: 60.22938298517763,
        cost: 1268.94
    },
    {
        long: 24.909403791938303,
        lat: 60.27137184187363,
        cost: 1318.97
    },
    {
        long: 24.966515262533783,
        lat: 60.124406772378094,
        cost: 1378.52
    },
    {
        long: 25.058327413194554,
        lat: 60.26817321291683,
        cost: 1410.86
    },
    {
        long: 24.867220439482665,
        lat: 60.25779687068884,
        cost: 1515.35
    },
    {
        long: 25.091974944462542,
        lat: 60.158649043308316,
        cost: 1573.2
    },
    {
        long: 25.104341301711695,
        lat: 60.19650567619238,
        cost: 1626.41
    },
    {
        long: 24.879405860490206,
        lat: 60.131060466467076,
        cost: 1636.16
    },
    {
        long: 24.899428578005566,
        lat: 60.15158796280313,
        cost: 1649.61
    },
    {
        long: 25.05193136287915,
        lat: 60.231216346409454,
        cost: 1678.48
    },
    {
        long: 24.903431118737007,
        lat: 60.14061889511821,
        cost: 1686.58
    },
    {
        long: 24.973523677039978,
        lat: 60.22227855215559,
        cost: 1813.1
    },
    {
        long: 24.858568230335585,
        lat: 60.277332688825005,
        cost: 1820.32
    },
    {
        long: 25.063817256551044,
        lat: 60.17784104194108,
        cost: 2234.17
    },
    {
        long: 25.0238457221579,
        lat: 60.251453066427615,
        cost: 2255.16
    },
    {
        long: 24.938135173249805,
        lat: 60.225778862853865,
        cost: 2326.24
    },
    {
        long: 25.094163833716827,
        lat: 60.185861819463184,
        cost: 2364.18
    },
    {
        long: 24.872082657044043,
        lat: 60.26652674991629,
        cost: 2630.61
    },
    {
        long: 24.994137402995687,
        lat: 60.201170836114116,
        cost: 2789.7
    },
    {long: 25.134263606407, lat: 60.12062365333369, cost: 2810.94},
    {
        long: 24.86922682340202,
        lat: 60.18196946315326,
        cost: 2893.74
    },
    {
        long: 25.02173003958548,
        lat: 60.20722857251636,
        cost: 3362.43
    },
    {long: 25.07699456608086, lat: 60.15764648278012, cost: 3410.4},
    {
        long: 24.841830295690155,
        lat: 60.17452166893983,
        cost: 3529.58
    },
    {long: 24.97933098877605, lat: 60.25492259358448, cost: 3757.5},
    {long: 25.01554009435494, lat: 60.1829838475861, cost: 3826},
    {
        long: 24.998024483820988,
        lat: 60.210241975023074,
        cost: 3952.15
    },
    {
        long: 24.87852525559632,
        lat: 60.12968569670284,
        cost: 4136.94
    },
    {
        long: 25.04237820239173,
        lat: 60.17458026070629,
        cost: 4832.05
    },
    {
        long: 25.04089601494082,
        lat: 60.177281927708854,
        cost: 6097.18
    },
    {
        long: 25.134900385859755,
        lat: 60.258437248126995,
        cost: 6475.64
    },
    {
        long: 24.956469448449184,
        lat: 60.26779315472639,
        cost: 6697.01
    },
    {
        long: 25.042016554480398,
        lat: 60.247410906586076,
        cost: 7604.46
    },
    {
        long: 24.973924387786628,
        lat: 60.2032137933498,
        cost: 8733.46
    },
    {
        long: 24.85498202130853,
        lat: 60.23167609874592,
        cost: 9163.99
    },
    {
        long: 25.048649318557075,
        lat: 60.19938901534546,
        cost: 9816.62
    },
    {
        long: 25.057118222355196,
        lat: 60.23174544340465,
        cost: 16565.72
    }];

let options = {
    'features': {
        'entities': {
            'emotion': true,
            'sentiment': true,
            'limit': 2
        },
        'keywords': {
            'emotion': true,
            'sentiment': true,
            'limit': 2
        },
        'concepts': {},
        'categories': {},
        'emotion': {},
        'relations': {},
        'semantic_roles': {},
        'sentiment': {}
    }
};

let nlu = new naturalLanguageUnderstandingV1({
    username: process.env.SERVICE_NAME_USERNAME,
    password: process.env.SERVICE_NAME_PASSWORD,
    version_date: naturalLanguageUnderstandingV1.VERSION_DATE_2017_02_27
});

const getAnalyzes = async (req, res) => {

    console.log('Start!');

    if (!util.isArray(req.body)) {
        let array = [];
        array.push(req.body);
        req.body = array;
    }

    for (let j = 0; j < req.body.length; j++) {
        let data = {};
        data.message = req.body[j].description;
        data.model = 'nmt';
        data.target = 'en';
        data.source = 'fi';

        req.body[j].description_en = await getTranslation2(data);
        req.body[j].analyze = await nlu2(req.body[j].description_en);

        console.log('lat: ' + req.body[j].lat);
        console.log('long: ' + req.body[j].long);

        let cost = 0;
        let count = 0;

        let radius = 1 / 100; // 1/100

        for (let x = 0; x < costs.length; x++) {

            if (costs[x].lat > req.body[j].lat - radius && costs[x].lat < req.body[j].lat + radius) {
                if (costs[x].long > req.body[j].long - radius && costs[x].long < req.body[j].long + radius) {
                    console.log('Inside area!');
                    count++;
                    cost = cost + costs[x].cost;
                }
            }
        }

        console.log('cost: ' + cost + ', count: ' + count);

        if (cost === 0) {
            req.body[j].avg_cost = cost;
        } else {
            req.body[j].avg_cost = cost / count;
        }
    }

    let response = {};
    response.success = true;
    response.message = req.body;
    return res.status(200).send(response);

};

const getDataAndAnalyze = async (req, res) => {

    console.log('Start2!');

    req.body = await getData();

    if (!util.isArray(req.body)) {
        let array = [];
        array.push(req.body);
        req.body = array;
    }

    for (let j = 0; j < req.body.length; j++) {

        let data = {};
        data.message = req.body[j].description;
        data.model = 'nmt';
        data.target = 'en';
        data.source = 'fi';

        req.body[j].description_en = await getTranslation2(data);
        req.body[j].analyze = await nlu2(req.body[j].description_en);

        // console.log('lat: ' + req.body[j].lat);
        // console.log('long: ' + req.body[j].long);

        let cost = 0;
        let count = 0;

        let radius = 1 / 100; // 1/100

        for (let x = 0; x < costs.length; x++) {

            if (costs[x].lat > req.body[j].lat - radius && costs[x].lat < req.body[j].lat + radius) {
                if (costs[x].long > req.body[j].long - radius && costs[x].long < req.body[j].long + radius) {
                    console.log('Inside area!');
                    count++;
                    cost = cost + costs[x].cost;
                }
            }
        }

        console.log('cost: ' + cost + ', count: ' + count);

        if (cost === 0) {
            req.body[j].avg_cost = cost;
        } else {
            req.body[j].avg_cost = cost / count;
        }
    }

    let response = {};
    response.success = true;
    response.message = req.body;
    return res.status(200).send(response);

};

const nlu2 = function (text) {
    let input = options;
    options.html = text;

    return new Promise(resolve => {
        return nlu.analyze(input, function (err, response) {
            if (err) {
                // console.log('error:', err);
                resolve({});
            } else {
                resolve(response);
            }
        });
    });
};

module.exports.analyze = function (req, res) {
    return getAnalyzes(req, res);
};

module.exports.get = function (req, res) {
    return getDataAndAnalyze(req, res);
};

