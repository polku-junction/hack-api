"use strict";
const getTranslation = require('../lib/google-translate').getTranslation;
const parseXHTMLString = require('../lib/google-translate').parseXHTMLString;

module.exports.translate = function (req, res) {
    getTranslation(req.body, function callback(error, response, body) {

        if (error || !response || response.statusCode !== 200) return res.status(500).send({success: false, message: 'Failure'});
        let parsedBody = null;

        try {
            parsedBody = JSON.parse(body);
        } catch (e) {
            console.log('Could not parse response from Google: ' + (body || 'null'));
            return res.status(500).send({success: false, message: 'Failure'});
        }

        if (!parsedBody.data.translations[0].translatedText) return;

        let data = {};
        data.success = true;
        data.message = parseXHTMLString(parsedBody.data.translations[0].translatedText);

        res.status(200).send(data);
    });
};