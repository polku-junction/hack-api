"use strict";
const util = require('util');
const async = require('async');
const rp = require('request-promise');
const getTranslation2 = require('../lib/google-translate').getTranslation2;
const getData = require('../lib/google-translate').getData;
const parseXHTMLString = require('../lib/google-translate').parseXHTMLString;
const naturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');

let options = {
    'features': {
        'entities': {
            'emotion': true,
            'sentiment': true,
            'limit': 2
        },
        'keywords': {
            'emotion': true,
            'sentiment': true,
            'limit': 2
        },
        'concepts': {},
        'categories': {},
        'emotion': {},
        'relations': {},
        'semantic_roles': {},
        'sentiment': {}
    }
};

let nlu = new naturalLanguageUnderstandingV1({
    username: process.env.SERVICE_NAME_USERNAME,
    password: process.env.SERVICE_NAME_PASSWORD,
    version_date: naturalLanguageUnderstandingV1.VERSION_DATE_2017_02_27
});

const getAnalyzes = async (req, res) => {

    console.log('Start!');

    if (!util.isArray(req.body)) {
        let array = [];
        array.push(req.body);
        req.body = array;
    }

    for (let j = 0; j < req.body.length; j++) {
        let data = {};
        data.message = req.body[j].description;
        data.model = 'nmt';
        data.target = 'en';
        data.source = 'fi';

        req.body[j].description_en = await getTranslation2(data);
        req.body[j].analyze = await nlu2(req.body[j].description_en);
    }

    let report = {
        concepts: {}
    };

    for (let j = 0; j < req.body.length; j++) {
        if (!req.body[j].analyze) continue;
        if (!req.body[j].analyze.concepts) continue;
        for (let c = 0; c < req.body[j].analyze.concepts.length; c++) {
            //console.log(req.body[j].analyze.keywords[c].text);
            if (report.concepts[req.body[j].analyze.concepts[c].text]) {
                //console.log('Found!');
                report.concepts[req.body[j].analyze.concepts[c].text]++;
            } else {
                console.log('Create!');
                report.concepts[req.body[j].analyze.concepts[c].text] = 1;
            }
        }
    }

    // Sorting
    let entries = Object.entries(report.concepts);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    report.concepts = {};
    for (let j = 0; j < sorted.length; j++) {
        report.concepts[sorted[j][0]] = sorted[j][1]
    }

    let response = {};
    response.success = true;
    // response.message = req.body;
    response.message = report;
    return res.status(200).send(response);

};

const getDataAndAnalyze = async (req, res) => {

    console.log('Start!');

    req.body = await getData();

    console.log(req.body);

    if (!util.isArray(req.body)) {
        let array = [];
        array.push(req.body);
        req.body = array;
    }

    for (let j = 0; j < req.body.length; j++) {
        let data = {};
        data.message = req.body[j].description;
        data.model = 'nmt';
        data.target = 'en';
        data.source = 'fi';

        req.body[j].description_en = await getTranslation2(data);
        req.body[j].analyze = await nlu2(req.body[j].description_en);
    }

    let report = {
        concepts: {}
    };

    for (let j = 0; j < req.body.length; j++) {
        if (!req.body[j].analyze) continue;
        if (!req.body[j].analyze.concepts) continue;
        for (let c = 0; c < req.body[j].analyze.concepts.length; c++) {
            //console.log(req.body[j].analyze.keywords[c].text);
            if (report.concepts[req.body[j].analyze.concepts[c].text]) {
                //console.log('Found!');
                report.concepts[req.body[j].analyze.concepts[c].text]++;
            } else {
                console.log('Create!');
                report.concepts[req.body[j].analyze.concepts[c].text] = 1;
            }
        }
    }

    // Sorting
    let entries = Object.entries(report.concepts);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    report.concepts = {};
    for (let j = 0; j < sorted.length; j++) {
        report.concepts[sorted[j][0]] = sorted[j][1]
    }

    let response = {};
    response.success = true;
    // response.message = req.body;
    response.message = report;
    return res.status(200).send(response);

};

const nlu2 = function (text) {
    let input = options;
    options.html = text;

    return new Promise(resolve => {
        return nlu.analyze(input, function (err, response) {
            if (err) {
                //console.log('error:', err);
                console.log('E');
                resolve({});
            } else {
                resolve(response);
            }
        });
    });
};

module.exports.pipe = function (req, res) {
    return getAnalyzes(req, res);
};

module.exports.get = function (req, res) {
    return getDataAndAnalyze(req, res);
};