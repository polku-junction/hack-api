"use strict";
const rp = require('request-promise');

var fs = require('fs');


const naturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
const getTranslation2 = require('../lib/google-translate').getTranslation2;

let nlu = new naturalLanguageUnderstandingV1({
    username: process.env.SERVICE_NAME_USERNAME,
    password: process.env.SERVICE_NAME_PASSWORD,
    version_date: naturalLanguageUnderstandingV1.VERSION_DATE_2017_02_27
});

let undecided =  0;
let rakenteellinen_kunnossapito = 0;
let viherpuoli = 0;
let talvihoito_puhtaanapito = 0;


let transANdAnalyze = function (obj) {

    let data = {};

    data.message = obj.message ;
    data.model = 'nmt';
    data.target = 'en';
    data.source = 'fi';

    return getTranslation2(data).then(result => {

        return new Promise(resolve => {

            return nlu.analyze({
                'html': result, // Buffer or String
                'features': {
                    'emotion':{},
                }
            }, function(err, response) {
                if (err) {
                    obj.sentiment = err;
                    resolve(obj);

                } else {
                    obj.sentiment = response;
                    resolve(obj);

                }
            });
        });

    })

};

let options = {
    method: 'GET',
    url: "https://asiointi.hel.fi/palautews/rest/v1/requests.json",
    json: true
};
let irrelevant_words = ['ja', 'on', 'ei', 'että', 'ole', 'mutta', 'jo',
    'ovat', 'niin', 'kohdalla', 'se', 'oli', 'kun',
    'kuin', 'ollut', 'ennen', 'olisi', 'eikä', 'olla', 'sen', 'nyt', 'liian',
    'vaan', 'myös', 'siinä', 'joka', 'tai', 'vaan', 'lisäksi', 'erittäin',
    'kaksi', 'vain', 'oleva', 'miksi', 'koko', 'vieressä', 'itse', 'aivan', 'jälkeen',
    '', 'en', 'voisi', 'voisiko', 'n', 'vai', 'joku', 'edes', 'olleet', 'tämä', 'onko', 'jonka',
    '-', 'miten', 'ihan', 'siitä', 'siltä', 'siihen', 'n.', 'näin', 'kuten', 'ko. '];

let rakenteellinen_kunnossapito_arr = ['Liikennemerkki',
    'Merkki', 'merki' , 'Merkinnät', 'Kaivo', 'Kuoppa', 'Kuoppia', 'Lisäkilpi', 'Reuna', 'Kivet', 'Reunakivet', 'Pientare', 'Ajorata',
    'Asfaltti', 'Asfaltissa', 'Viemäri', 'Pyöräilijä', 'Auto', 'Suojatien', 'Pyörätie', 'Autot', 'Pimeänä',
    'rikki', 'vinossa', 'hidastetöyssy', 'tlppa', 'sähkö', 'myrsky', 'syvä', 'tukkii', 'tukossa', 'tunneli', 'putki', 'katulamppu',
    'kansi', 'jalkakäytävä', 'lisäkilpi', 'ääni', 'reikä', 'näkyvyys', 'estää', 'vedessä',
    'lehdet', 'kovaäänisesti', 'remontti','remontoidaan', 'kaista', 'lava', 'lavoja'];

let talvihoito_puhtaanapito_arr = [
    'Auraus', 'Lumi', 'Hiekoitus', 'Hiekka', 'Sora', 'Liukas', 'Suolaus', 'Suola', 'Roska', 'Roskia', 'Roskat', 'Pöly',
    'Jäätynyt', 'Lumi', 'Lumikasat' , 'sepeli', 'jää', 'tyhjennys', 'luomihuoli'];


let viherpuoli_arr  = ['Puisto', 'Nurmi', 'Ruohikko', 'Ruoho', 'Leikki', 'Leikkikenttä', 'Leikkipuisto', 'Puunoksa', 'Oksat',
    'Huumeneulat', 'Neulat', 'Roskis', 'Roskakori', 'Puista', 'Lehdet', 'Puistossa', 'Puiston', 'Puu',
    'inputslehti', 'jättää', 'jätetty', 'poikki', 'puisto', 'leikki', 'lapsi', 'viheralue', 'viher', 'siivota'
];


let objCreator = function (message) {

    let obj = {
        "keywords":'',
        "category":'',
        "message":''
    };

    obj.message = message;
    let inputWords = message.split(" ");

    let resultArr = [];
    let common1 = intersectFinder(inputWords, rakenteellinen_kunnossapito_arr);
    let common2 = intersectFinder(inputWords, talvihoito_puhtaanapito_arr);
    let common3 = intersectFinder(inputWords, viherpuoli_arr);

    resultArr.push({category:'rakenteellinen_kunnossapito', keywords: common1, count:common1.length });
    resultArr.push({category:'talvihoito_puhtaanapito', keywords: common2, count:common2.length });
    resultArr.push({category:'viherpuoli', keywords: common3, count:common3.length });
    resultArr.sort(compare);
    let highestCount = resultArr[resultArr.length - 1].count;
    let keywords_from_selected = resultArr[resultArr.length - 1].keywords;


    if(highestCount === 0){
        obj.category = "undecided";
        ++undecided;
    } else {

        switch (resultArr[resultArr.length - 1].category){
            case "rakenteellinen_kunnossapito":
                obj.category = "rakenteellinen_kunnossapito";
                ++rakenteellinen_kunnossapito;
                break;
            case "talvihoito_puhtaanapito":
                obj.category = "talvihoito_puhtaanapito";
                ++talvihoito_puhtaanapito;
                break;
            case "viherpuoli":
                obj.category = "viherpuoli";
                ++viherpuoli;
                break;
            default:
                console.log("this is result arr which couldnt undentified " + resultArr[resultArr.length - 1].category);
        }
        obj.keywords = keywords_from_selected;

    }
    return obj;
};


module.exports.categorizer = function (req, res) {

    let inputs = req.body.inputs;
    if(!inputs){
        return res.status(400).send("inputs is undefined");
    }
    undecided =  0;
    rakenteellinen_kunnossapito = 0;
    viherpuoli = 0;
    talvihoito_puhtaanapito = 0;


    let allPromises = [];

    for(let i = 0; i < inputs.length; i++) {
            let obj = objCreator(inputs[i].description);
            allPromises.push(transANdAnalyze(obj));
    }

    console.log("query results: undecided " +undecided);
    console.log("rakenteellinen_kunnossapito " +rakenteellinen_kunnossapito);
    console.log("viherpuoli " +viherpuoli);
    console.log("talvihoito_puhtaanapito " + talvihoito_puhtaanapito);

    return Promise.all(allPromises).then(result => {
        res.status(200).send(result).end();

    }).catch(error => {
        res.status(400).send(error).end();
    })
};


function  getExisitngs() {
    return rp(options).then(function (APIresponse) {

        undecided =  0;
        rakenteellinen_kunnossapito = 0;
        viherpuoli = 0;
        talvihoito_puhtaanapito = 0;

        let allObjects = [];
        let length = APIresponse.length;

        for(let i = 0; i < length; i++) {
            let obj = objCreator(APIresponse[i].description);
            allObjects.push(obj)
        }
        console.log("just queried all database. final results: undecided " +undecided);
        console.log("rakenteellinen_kunnossapito " +rakenteellinen_kunnossapito);
        console.log("viherpuoli " +viherpuoli);
        console.log("talvihoito_puhtaanapito " + talvihoito_puhtaanapito);

        return Promise.resolve(allObjects);
    }).catch(function (error) {
        return Promise.reject(error);
    });
}

getExisitngs();


module.exports.existing = function (req,res) {
        return getExisitngs().then(result => {
            res.send(result);
        }).catch(error => {
            res.send(error);
        })
};




// a should be inputs, and b should be refrences keywords
function intersectFinder(a, b) {
    return a.filter(function (e) {
        for(let i=0; i < b.length; i++){
            let lower = b[i].toLowerCase();
            if(e.toLowerCase().indexOf(lower) !== -1) {
                return true;
            }
        }
        return false;
    });
}


function compare(a, b) {
    if (a.count < b.count) {
        return -1;
    }
    if (a.count > b.count) {
        return 1;
    }
    // a must be equal to b
    return 0;
}


